import { renderHook } from '@testing-library/react';
import { useDarkMode } from '../useDarkMode';

describe('useDarkMode', () => {
    it('uses prefers-color-scheme media query', () => {
        let listeners: ((e: MediaQueryListEvent) => void)[] = [];
        // TypeScript expects that matchMedia returns MediaQueryList, not void.
        // @ts-ignore
        window.matchMedia = jest.fn().mockImplementation((query) => ({
            matches: false,
            media: query,
            addEventListener: jest.fn((event, handler) => {
                listeners.push(handler);
            }),
            removeEventListener: jest.fn((event, handler) => {
                listeners = listeners.filter((listener) => listener !== handler);
            }),
        }));

        const { result } = renderHook(() => useDarkMode());

        expect(window.matchMedia).toHaveBeenCalledWith('(prefers-color-scheme: dark)');
        expect(result.current.darkMode).toBe(false);
    });
});