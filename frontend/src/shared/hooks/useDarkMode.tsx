import { useEffect, useState } from "react";

export const useDarkMode = () => {
    const [darkMode, setDarkMode] = useState(localStorage.getItem('darkMode') === 'true');

    useEffect(() => {
        const updateLocalStorage = () => {
            localStorage.setItem('darkMode', darkMode.toString());
        };
        const darkModeQuery = window.matchMedia("(prefers-color-scheme: dark)");
        setDarkMode(darkModeQuery.matches);
        const updateDarkMode = (e: MediaQueryListEvent) => {
            setDarkMode(e.matches);
            updateLocalStorage();
        };

        darkModeQuery.addEventListener("change", updateDarkMode);

        return () => {
            darkModeQuery.removeEventListener("change", updateDarkMode);
        };
    }, []);

    return { darkMode };
};