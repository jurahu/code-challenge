import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom";
import { PokemonTile } from "../PokemonTile";
import { PokemonResult } from "@/actions/getPokemons";
import userEvent from "@testing-library/user-event";

// Mock the Next.js Link component to avoid errors since we're not in an app-routing environment
jest.mock("next/link", () => ({ children }: { children: any }) => {
    return children;
});

// Prepare a mock Pokemon object
const mockPokemon: PokemonResult = {
    id: "001",
    name: "Bulbasaur",
    image: "bulbasaur.png",
    types: ["Grass", "Poison"],
    isFavorite: false,
};

// Write a test for the "tile" view type
describe("PokemonTile", () => {
    it("renders correctly as a tile", () => {
        const mockToggleFavorite = jest.fn();
        const { container } = render(<PokemonTile pokemon={mockPokemon} onToggleFavorite={mockToggleFavorite} viewType="tile" />);

        expect(container.querySelector(".tileWrapper")).toBeInTheDocument();
        expect(container.querySelector("h3")).toHaveTextContent(mockPokemon.name);
        expect(container.querySelector("p")).toHaveTextContent("Grass, Poison");
        expect(screen.getByRole("button", { name: /favorite/i })).toBeInTheDocument();

        // Check if images are rendered
        const images = container.querySelectorAll("img");
        expect(images).toHaveLength(1);
        expect(images[0]).toHaveAttribute("src", mockPokemon.image);
    });

    it("calls onToggleFavorite when favorite button is clicked", async () => {
        const mockToggleFavorite = jest.fn(() => Promise.resolve());
        render(<PokemonTile pokemon={mockPokemon} onToggleFavorite={mockToggleFavorite} viewType="tile" />);

        const favoriteButton = screen.getByRole("button", { name: /favorite/i });
        userEvent.click(favoriteButton);

        await waitFor(() => expect(mockToggleFavorite).toHaveBeenCalledTimes(1));
        expect(mockToggleFavorite).toHaveBeenCalledWith(mockPokemon.id, mockPokemon.isFavorite);
        // It should call the mockToggleFavorite function passing the right parameters
        expect(mockToggleFavorite).toHaveBeenCalledWith(mockPokemon.id, mockPokemon.isFavorite);
    });

    it("renders correctly as a row", () => {
        const mockToggleFavorite = jest.fn();
        const { container } = render(<PokemonTile pokemon={mockPokemon} onToggleFavorite={mockToggleFavorite} viewType="row" />);

        // Assert the row representation has been rendered as expected.
        // Similar assertions can be made as in the tile test, adapted for the row layout.
    });

    // Additional tests can be written for when the Pokemon is already a favorite,
    // to test that the filled favorite icon is displayed and the aria-label is correct.
});
