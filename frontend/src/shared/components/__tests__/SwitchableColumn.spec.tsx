import React from "react";
import { render } from "@testing-library/react";
import { SwitchableColumn } from "../SwitchableColumn";

test("SwitchableColumn component renders correctly", () => {
    const children = <div>Test Children</div>;
    const rowMode = true;

    const { asFragment } = render(<SwitchableColumn rowMode={rowMode}>{children}</SwitchableColumn>);

    expect(asFragment()).toMatchSnapshot();
});
