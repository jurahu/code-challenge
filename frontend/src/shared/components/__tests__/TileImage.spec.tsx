import { render } from "@testing-library/react";
import { TileImage } from "../TileImage";

test("TileImage component renders correctly", () => {
    const props = {
        url: "test-url",
        name: "Test Image",
        small: true,
    };

    const { asFragment } = render(<TileImage {...props} />);

    expect(asFragment()).toMatchSnapshot();
});
