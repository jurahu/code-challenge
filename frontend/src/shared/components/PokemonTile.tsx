"use client";
import styles from "./PokemonTile.module.scss";
import { Button, Stack, Tile } from "@carbon/react";
import { Favorite, FavoriteFilled } from "@carbon/icons-react";
import { useCallback } from "react";
import { TileImage } from "@/shared/components/TileImage";
import { PokemonResult } from "@/actions/getPokemons";
import Link from "next/link";

interface PokemonTileProps {
    pokemon: PokemonResult;
    onToggleFavorite: (id: string, isFavorite: boolean) => Promise<void>;
    viewType: "tile" | "row";
}

export function PokemonTile({ pokemon, onToggleFavorite, viewType }: PokemonTileProps) {
    const onFavoriteClick = useCallback(
        async function onFavoriteClick() {
            await onToggleFavorite(pokemon.id, pokemon.isFavorite);
        },
        [pokemon.id, pokemon.isFavorite, onToggleFavorite]
    );
    if (viewType === "row") {
        return (
            <div className={styles.tileWrapper}>
                <Tile>
                    <div className={styles.flex}>
                        <Link href={`/detail/${pokemon.id}`}>
                            <TileImage url={pokemon.image} name={pokemon.name} small={true} />
                        </Link>
                        <div className={styles.main}>
                            <Stack gap={1} className={styles.mainRow}>
                                <h3>{pokemon.name}</h3>
                                {pokemon.types ? <p>{pokemon.types?.join(", ")}</p> : null}
                            </Stack>
                        </div>
                        <Button
                            label={pokemon.isFavorite ? "unfavorite" : "favorite"}
                            aria-label={pokemon.isFavorite ? "unfavorite" : "favorite"}
                            hasIconOnly={true}
                            onClick={onFavoriteClick}
                            className={styles.favoriteButton}
                        >
                            {pokemon.isFavorite ? <FavoriteFilled /> : <Favorite />}
                        </Button>
                    </div>
                </Tile>
            </div>
        );
    }
    return (
        <div className={styles.tileWrapper}>
            <Tile>
                <Stack orientation="vertical" gap={5}>
                    <Link href={`/detail/${pokemon.id}`}>
                        <TileImage url={pokemon.image} name={pokemon.name} small={false} />
                    </Link>
                    <div className={styles.flex}>
                        <Stack gap={1} className={styles.main}>
                            <h3>{pokemon.name}</h3>
                            {pokemon.types ? <p>{pokemon.types.join(", ")}</p> : null}
                        </Stack>
                        <Button
                            hasIconOnly={true}
                            label={pokemon.isFavorite ? "unfavorite" : "favorite"}
                            aria-label={pokemon.isFavorite ? "unfavorite" : "favorite"}
                            onClick={onFavoriteClick}
                            size="md"
                            className={styles.favoriteButton}
                        >
                            {pokemon.isFavorite ? <FavoriteFilled /> : <Favorite />}
                        </Button>
                    </div>
                </Stack>
            </Tile>
        </div>
    );
}
