import styles from "./TileImage.module.scss";

type TileImageProps = { url: string; name: string; small: boolean };

export function TileImage({ url, name, small }: TileImageProps) {
    return (
        <div className={small ? styles.small : styles.large}>
            <img className={styles.image} src={url} alt={name} aria-label={`Image ${name}`} />
        </div>
    );
}
