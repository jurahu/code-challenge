"use client";

import { GlobalTheme, Theme } from "@carbon/react";
import { useDarkMode } from "@/shared/hooks/useDarkMode";

export function ThemeSwitcher({ children }: Readonly<{ children: React.ReactNode }>) {
    const { darkMode } = useDarkMode();
    return (
        <GlobalTheme>
            <Theme theme={darkMode ? "g100" : "white"}>{children}</Theme>
        </GlobalTheme>
    );
}
