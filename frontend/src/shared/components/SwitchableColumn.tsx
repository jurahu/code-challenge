import { Column } from "@carbon/react";

export function SwitchableColumn({ children, rowMode }: { children: React.ReactNode; rowMode: boolean }) {
    return (
        <Column lg={rowMode ? 16 : 4} md={rowMode ? 16 : 8} sm={rowMode ? 16 : 8} span={16}>
            {children}
        </Column>
    );
}
