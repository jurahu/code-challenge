"use server";

import { BACKEND_URL } from "@/api/backendUrl";
import { Pokemon } from "@/api/graphql";

interface GetPokemonsOptions {
    isFavorite?: boolean;
    search?: string;
    type?: string;
    limit?: number;
    offset?: number;
}

export type PokemonResult = Pick<Pokemon, 'id' | 'name' | 'image' | 'isFavorite' | 'types'>;

function isPokemonArray(pokemons: any): pokemons is PokemonResult[] {
    if (pokemons && pokemons.length === 0) {
        return true;
    }
    return pokemons && pokemons[0].id !== undefined;
}

const DEFAULT_PAGE_SIZE = 12;

export async function getPokemons(options?: GetPokemonsOptions) {
    const limit = options?.limit ?? DEFAULT_PAGE_SIZE;
    const offset = options?.offset ?? 0;

    const query = `
        query GetPokemons($limit: Int, $offset: Int, $search: String, $isFavorite: Boolean, $type: String) {
            pokemons(
                query: {
                    limit: $limit,
                    offset: $offset,
                    search: $search,
                    filter: { type: $type, isFavorite: $isFavorite }
                }
            ) {
                count,
                edges {
                    id,
                    name,
                    image,
                    isFavorite,
                    types
                }
            }
        }
    `;

    const variables = {
        limit,
        offset,
        search: options?.search,
        isFavorite: options?.isFavorite,
        type: options?.type,
    };

    const res = await fetch(BACKEND_URL, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            query,
            variables, // Send the variables as part of the request
        }),
    });

    if (!res.ok) {
        throw new Error("Failed to fetch data");
    }

    const result = await res.json();
    const pokemons = result.data?.pokemons?.edges;
    if (!pokemons) {
        throw new Error("Failed to fetch data, no data");
    }
    if (!isPokemonArray(pokemons)) {
        throw new Error("Failed to fetch data, bad format");
    }

    return pokemons;
}
