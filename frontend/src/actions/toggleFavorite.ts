"use server";

import { BACKEND_URL } from "@/api/backendUrl";

export async function toggleFavorite(id: string, isFavorite: boolean) {
    const FavoriteQuery = `mutation FavoritePokemon($id: ID!) {
                favoritePokemon(id: $id) {
                    id
                    name
                    isFavorite
                }
            }`;
    const unFavoriteQuery = `mutation UnFavoritePokemon($id: ID!) {
                unFavoritePokemon(id: $id) {
                    id
                    name
                    isFavorite
                }
            }`;

    const res = await fetch(BACKEND_URL, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            query: isFavorite ? unFavoriteQuery : FavoriteQuery,
            variables: {
                id: id,
            },
        }),
    });

    if (!res.ok) {
        throw new Error("Failed to toggle Favorite");
    }
}
