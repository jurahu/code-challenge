"use server";

import { BACKEND_URL } from "@/api/backendUrl";
import { Pokemon } from "@/api/graphql";

const DEFAULT_PAGE_SIZE = 12;

export type PokemonDetailResult = Pick<
    Pokemon,
    "id" | "name" | "image" | "isFavorite" | "types" | "maxCP" | "maxHP" | "sound" | "height" | "weight" | "evolutions"
>;

export async function getPokemonById(id: string) {
    const query = `
        query PokemonsById($id: ID!) {
            pokemonById(id: $id) {
                id,
                name,
                image,
                isFavorite,
                types,
                maxCP,
                maxHP,
                sound,
                height {
                    minimum,
                    maximum
                },
                weight {
                    minimum,
                    maximum
                },
                evolutions {
                    id
                    name,
                    isFavorite
                    image
                }
            }
        }
    `;

    const variables = { id };

    const res = await fetch(BACKEND_URL, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            query,
            variables, // Send the variables as part of the request
        }),
    });

    if (!res.ok) {
        throw new Error("Failed to fetch data");
    }

    const result = await res.json();
    const pokemon = result.data?.pokemonById as PokemonDetailResult;
    if (!pokemon) {
        throw new Error("Failed to fetch data, no data");
    }

    return pokemon;
}
