"use server";

import { BACKEND_URL } from "@/api/backendUrl";

export async function getPokemonTypes() {
    const query = `
        query { pokemonTypes }
    `;

    const res = await fetch(BACKEND_URL, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            query,
        }),
    });

    if (!res.ok) {
        throw new Error("Failed to fetch data");
    }

    const result = await res.json();
    const pokemonTypes = result.data?.pokemonTypes;
    if (!pokemonTypes) {
        throw new Error("Failed to fetch data, no data");
    }

    return pokemonTypes;
}
