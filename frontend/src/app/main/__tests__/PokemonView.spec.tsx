import React from "react";
import { act, render } from "@testing-library/react";
import { PokemonView } from "../PokemonView";

jest.mock("../../../actions/getPokemons", () => ({
    getPokemons: jest.fn().mockImplementation((options) => {
        // Your dynamic mock implementation based on the input parameters
        if (options.offset === 0) {
            return Promise.resolve([
                {
                    id: "1",
                    name: "Bulbasaur",
                    types: ["Grass", "Poison"],
                    image: "fakeimageurl",
                    isFavorite: false,
                },
            ]);
        } else {
            return Promise.resolve([{ id: "2", name: "Charmeleon", types: ["Fire"], image: "fakeimageurl", isFavorite: false }]);
        }
    }),
}));

describe("PokemonView", () => {
    it("renders view with data", async () => {
        const pokemonTypes = ["Grass"];
        const pokemonsInit = [
            {
                id: "1",
                name: "Bulbasaur",
                types: ["Grass", "Poison"],
                image: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png",
                isFavorite: false,
            },
        ];

        const onToggleFavorite = jest.fn();
        let component: any;
        await act(async () => {
            component = render(<PokemonView pokemonTypes={pokemonTypes} pokemonsInit={pokemonsInit} toggleFavorite={onToggleFavorite} />);
        });
        expect(component?.baseElement).toMatchSnapshot();
    });
});

