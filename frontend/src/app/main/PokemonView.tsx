"use client";
import styles from "@/app/main/PokemonView.module.scss";
import {
    Button,
    Column,
    Content,
    ContentSwitcher,
    Dropdown,
    Grid,
    Header,
    HeaderName,
    IconSwitch,
    Loading,
    Search,
    SkipToContent,
    Stack,
    Tab,
    TabList,
    Tabs,
} from "@carbon/react";
import { useCallback, useEffect, useState } from "react";
import { PokemonTile } from "@/shared/components/PokemonTile";
import { Column as ColumnIcon, List as ListIcon } from "@carbon/icons-react";
import { getPokemons, PokemonResult } from "@/actions/getPokemons";
import { useInfiniteScroll } from "@/app/main/hooks/useInfiniteScroll";
import { useViewStyle } from "@/app/main/hooks/useViewStyle";
import { TYPE_ALL, usePokemonTypeSelect } from "@/app/main/hooks/usePokemonTypeSelect";
import { useSelectTab } from "@/app/main/hooks/useSelectTab";
import { useSearch } from "@/app/main/hooks/useSearch";
import { toggleFavorite } from "@/actions/toggleFavorite";
import { SwitchableColumn } from "@/shared/components/SwitchableColumn";

interface PokemonListProps {
    pokemonsInit: PokemonResult[];
    pokemonTypes: string[];
    toggleFavorite: (id: string, isFavorite: boolean) => Promise<void>;
}

export function PokemonView({ pokemonsInit, pokemonTypes }: PokemonListProps) {
    const pokemonTypesWithAll = [TYPE_ALL, ...pokemonTypes];

    const onToggleFavorite = useCallback(async function onFavoriteClick(id: string, isFavorite: boolean) {
        await toggleFavorite(id, isFavorite);
        setPokemonsData((prevState) =>
            prevState.map((pokemon) => {
                if (pokemon.id === id) {
                    return { ...pokemon, isFavorite: !isFavorite };
                }
                return pokemon;
            })
        );
    }, []);
    const { selectedPokemonType, onSelectType } = usePokemonTypeSelect();
    const { selectedTabIndex, onSelectTab } = useSelectTab();
    const { search, onSearchChange } = useSearch();
    const { viewStyle, onViewStyleSwitch } = useViewStyle();

    const [offset, setOffset] = useState(pokemonsInit.length);
    const [pokemonsData, setPokemonsData] = useState(pokemonsInit);
    useEffect(() => {
        const fetchData = async () => {
            scroll.setLoading(true);
            scroll.setHasMore(true);
            const newPokemonData = await getPokemons({
                offset: 0,
                search,
                // in case of all, undefined is sent to the server so we don't filter any
                type: selectedPokemonType === TYPE_ALL ? undefined : selectedPokemonType,
                isFavorite: selectedTabIndex === 1,
            });
            scroll.setHasMore(newPokemonData.length > 0);
            scroll.setLoading(false);
            setOffset(newPokemonData.length);
            setPokemonsData(newPokemonData);
        };
        fetchData();
    }, [search, selectedTabIndex, selectedPokemonType]);

    const loadMorePokemons = async () => {
        try {
            scroll.setLoading(true);
            const newPokemons = await getPokemons({
                offset: offset,
                search,
                type: selectedPokemonType === TYPE_ALL ? undefined : selectedPokemonType,
                isFavorite: selectedTabIndex === 1,
            });
            if (newPokemons.length > 0) {
                setPokemonsData((prevPokemons) => [...prevPokemons, ...newPokemons]);
                setOffset((prevOffset) => prevOffset + newPokemons.length);
            } else {
                scroll.setHasMore(false);
            }
        } catch (error) {
            console.error("Error loading more pokemons", error);
        } finally {
            scroll.setLoading(false);
        }
    };

    const onLoadMore = useCallback(loadMorePokemons, [search, selectedTabIndex, selectedPokemonType]);
    const scroll = useInfiniteScroll(loadMorePokemons);

    return (
        <div ref={scroll.scrollContainerRef} style={{ overflowY: "auto", height: "100vh" }}>
            <Header aria-label="Pokemon Header">
                <SkipToContent />
                <HeaderName href="/" prefix="">
                    Pokemon
                </HeaderName>
                <Loading className={"some-class"} withOverlay={false} small={true} active={scroll.loading} />
            </Header>
            <Content>
                <Stack gap={8}>
                    <Grid>
                        <Column span={16} className="cols">
                            <Tabs selectedIndex={selectedTabIndex} onChange={onSelectTab}>
                                <TabList aria-label="Tab Selector All or Favorites">
                                    <Tab>All</Tab>
                                    <Tab>Favorites</Tab>
                                </TabList>
                            </Tabs>
                        </Column>
                    </Grid>
                    <Grid>
                        <Column span={16}>
                            <Stack gap={6} orientation="horizontal">
                                <Search
                                    placeholder="Find your items"
                                    labelText="Search"
                                    closeButtonLabelText="Clear search input"
                                    id="search-1"
                                    onChange={onSearchChange}
                                />
                                <Dropdown
                                    id="default"
                                    type="inline"
                                    titleText="Type"
                                    onChange={onSelectType}
                                    initialSelectedItem={TYPE_ALL}
                                    label={TYPE_ALL}
                                    items={pokemonTypesWithAll}
                                />

                                <ContentSwitcher size="md" selectedIndex={viewStyle} onChange={onViewStyleSwitch}>
                                    <IconSwitch name="column" text="Column">
                                        <ColumnIcon />
                                    </IconSwitch>
                                    <IconSwitch name="list" text="List">
                                        <ListIcon />
                                    </IconSwitch>
                                </ContentSwitcher>
                            </Stack>
                        </Column>

                        <Column span={16}>
                            <Stack gap={1}>
                                <Grid>
                                    {pokemonsData.map((pokemon) => (
                                        <SwitchableColumn key={pokemon.id} rowMode={viewStyle === 1}>
                                            <PokemonTile
                                                pokemon={pokemon}
                                                onToggleFavorite={onToggleFavorite}
                                                viewType={viewStyle === 0 ? "tile" : "row"}
                                            />
                                        </SwitchableColumn>
                                    ))}
                                </Grid>
                            </Stack>
                        </Column>

                        <Column span={16} className={styles.loadMore}>
                            {scroll.hasMore && <Button onClick={onLoadMore}>Load More</Button>}
                        </Column>
                    </Grid>
                </Stack>
            </Content>
        </div>
    );
}
