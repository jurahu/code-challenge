import { useEffect, useRef, useState } from "react";

export function useInfiniteScroll(callback: () => void) {
    const scrollContainerRef = useRef<HTMLDivElement>(null);
    const [loading, setLoading] = useState(false);
    const [hasMore, setHasMore] = useState(true);

    useEffect(() => {
        const handleScroll = () => {
            const { scrollTop, clientHeight, scrollHeight } = scrollContainerRef.current!;
            if (scrollHeight - scrollTop === clientHeight && !loading && hasMore) {
                callback();
            }
        }

        // Add event listener only if the ref is currently pointing to a DOM element
        if (scrollContainerRef.current) {
            scrollContainerRef.current.addEventListener("scroll", handleScroll);
        }

        // Cleanup function which removes the event listener
        return () => {
            // Ensure we check for null before trying to remove the listener
            if (scrollContainerRef.current) {
                scrollContainerRef.current.removeEventListener("scroll", handleScroll);
            }
        };
        // Since handleScroll is recreated on every render, it should be included in the dependency array
    }, [loading, hasMore]);

    // Rest of your hook logic...

    return { scrollContainerRef, loading, setLoading, setHasMore, hasMore };
}