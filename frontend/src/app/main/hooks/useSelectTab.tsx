import { useCallback, useState } from "react";

export function useSelectTab() {
    const [selectedTabIndex, setSelectedTabIndex] = useState(0);
    const onSelectTab = useCallback(function onFilterSwitch(event: { selectedIndex: number }) {
        setSelectedTabIndex(event.selectedIndex);
    }, []);
    return { selectedTabIndex, onSelectTab };
}