import { useCallback, useState } from "react";

export function useViewStyle() {
    const [viewStyle, setViewStyle] = useState(0);

    const onViewStyleSwitch = useCallback(function onViewStyleSwitch(event: { index?: number }) {
        if (event.index === undefined) return;
        setViewStyle(event.index);
    }, []);

    return { viewStyle, onViewStyleSwitch };
}
