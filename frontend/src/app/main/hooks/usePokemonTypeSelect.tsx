import { useCallback, useState } from "react";
import { OnChangeData } from "@carbon/react/lib/components/Dropdown/Dropdown";

export const TYPE_ALL = "All";

export function usePokemonTypeSelect() {
    const [selectedPokemonType, setSelectedPokemonType] = useState<string>(TYPE_ALL);
    const onSelectType = useCallback(function onSelectType(data: OnChangeData<string>) {
        setSelectedPokemonType(data.selectedItem ?? TYPE_ALL);
    }, []);
    return { selectedPokemonType, onSelectType };
}
