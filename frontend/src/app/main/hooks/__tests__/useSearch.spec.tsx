import { act, renderHook } from "@testing-library/react";
import { useSearch } from "@/app/main/hooks/useSearch"; // Adjust the import path as necessary

describe('useSearch custom hook', () => {
    it('should update search state after debounce', () => {
        // Use fake timers to control setTimeout
        jest.useFakeTimers();

        // Render the hook in an isolated test environment
        const { result } = renderHook(() => useSearch());

        act(() => {
            // Simulate onSearchChange with a new value
            result.current.onSearchChange({
                target: { value: "test query" },
                type: "change"
            });

            // Move the jest timer ahead to simulate the debounce delay
            jest.runAllTimers();
        });

        // Check the resulting search state value has been updated
        expect(result.current.search).toBe("test query");

        // Switch back to real timers
        jest.useRealTimers();
    });
});