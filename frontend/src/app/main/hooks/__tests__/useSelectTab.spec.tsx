import { act, render } from "@testing-library/react";
import { useSelectTab } from "../useSelectTab";

// Test component that uses the hook
function TestComponent() {
    const { selectedTabIndex, onSelectTab } = useSelectTab();
    return (
        <div>
            <div data-testid="selected-index">{selectedTabIndex}</div>
            <button onClick={() => onSelectTab({ selectedIndex: 1 })}>Select Tab 1</button>
            <button onClick={() => onSelectTab({ selectedIndex: 2 })}>Select Tab 2</button>
        </div>
    );
}

describe("useSelectTab hook", () => {
    it("should initialize with a default selected tab index of 0", () => {
        const { getByTestId } = render(<TestComponent />);
        const selectedIndexElement = getByTestId("selected-index");
        expect(selectedIndexElement.textContent).toBe("0");
    });

    it("should update selectedTabIndex when onSelectTab is called", () => {
        const { getByTestId, getByText } = render(<TestComponent />);
        const selectedIndexElement = getByTestId("selected-index");
        const selectTab1Button = getByText("Select Tab 1");
        const selectTab2Button = getByText("Select Tab 2");

        // Select tab 1
        act(() => {
            selectTab1Button.click();
        });
        expect(selectedIndexElement.textContent).toBe("1");

        // Select tab 2
        act(() => {
            selectTab2Button.click();
        });
        expect(selectedIndexElement.textContent).toBe("2");
    });
});
