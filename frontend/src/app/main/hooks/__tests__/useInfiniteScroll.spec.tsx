import React from "react";
import { act, fireEvent, render } from "@testing-library/react";
import { useInfiniteScroll } from "@/app/main/hooks/useInfiniteScroll";

// Mock container div to emulate the scrollable container
const ScrollableContainer = ({
    handleLoadMore,
    shouldHaveMore,
    isLoading,
}: {
    handleLoadMore: any;
    isLoading: boolean;
    shouldHaveMore: boolean;
}) => {
    const { scrollContainerRef, setLoading, setHasMore } = useInfiniteScroll(handleLoadMore);

    // Simulate external control
    React.useEffect(() => {
        setLoading(isLoading);
        setHasMore(shouldHaveMore);
    }, [isLoading, shouldHaveMore]);

    return (
        <div ref={scrollContainerRef} data-testid="scroll-container" style={{ height: "100px", overflowY: "auto" }}>
            <div style={{ height: "300px" }}>Content to scroll</div>
        </div>
    );
};

describe("useInfiniteScroll", () => {
    it("does not callback when not scrolled to bottom", () => {
        const handleLoadMore = jest.fn();
        const { getByTestId } = render(<ScrollableContainer handleLoadMore={handleLoadMore} isLoading={false} shouldHaveMore={true} />);
        const scrollContainer = getByTestId("scroll-container");

        // Fire a scroll event without reaching the bottom
        fireEvent.scroll(scrollContainer, { target: { scrollTop: 50 } });
        expect(handleLoadMore).not.toHaveBeenCalled();
    });

    it("calls callback when scrolled to bottom", () => {
        const handleLoadMore = jest.fn();
        const { getByTestId } = render(<ScrollableContainer handleLoadMore={handleLoadMore} isLoading={false} shouldHaveMore={true} />);
        const scrollContainer = getByTestId("scroll-container");

        // Fire a scroll event reaching the bottom
        act(() => {
            fireEvent.scroll(scrollContainer, {
                target: { scrollTop: scrollContainer.scrollHeight - scrollContainer.clientHeight },
            });
        });
        expect(handleLoadMore).toHaveBeenCalledTimes(1);
    });

    it("does not call callback when hasMore is false", () => {
        const handleLoadMore = jest.fn();
        const { getByTestId } = render(<ScrollableContainer handleLoadMore={handleLoadMore} isLoading={false} shouldHaveMore={false} />);
        const scrollContainer = getByTestId("scroll-container");

        // Fire a scroll event reaching the bottom
        act(() => {
            fireEvent.scroll(scrollContainer, {
                target: { scrollTop: scrollContainer.scrollHeight - scrollContainer.clientHeight },
            });
        });
        expect(handleLoadMore).not.toHaveBeenCalled();
    });

    it("does not call callback when isLoading", () => {
        const handleLoadMore = jest.fn();
        const { getByTestId } = render(<ScrollableContainer handleLoadMore={handleLoadMore} isLoading={true} shouldHaveMore={true} />);
        const scrollContainer = getByTestId("scroll-container");

        // Fire a scroll event reaching the bottom
        act(() => {
            fireEvent.scroll(scrollContainer, {
                target: { scrollTop: scrollContainer.scrollHeight - scrollContainer.clientHeight },
            });
        });
        expect(handleLoadMore).not.toHaveBeenCalled();
    });
});
