import { act, renderHook } from "@testing-library/react";
import { useViewStyle } from "../useViewStyle";

describe("useViewStyle hook", () => {
    it("should initialize with a default view style index of 0", () => {
        const { result } = renderHook(() => useViewStyle());
        expect(result.current.viewStyle).toBe(0);
    });

    it("should update viewStyle when onViewStyleSwitch is called with a new index", () => {
        const { result } = renderHook(() => useViewStyle());

        act(() => result.current.onViewStyleSwitch({ index: 1 }));
        expect(result.current.viewStyle).toBe(1);

        act(() => result.current.onViewStyleSwitch({ index: 2 }));
        expect(result.current.viewStyle).toBe(2);
    });

    it("should not update viewStyle when onViewStyleSwitch is called with an undefined index", () => {
        const { result } = renderHook(() => useViewStyle());

        // Initially, view style index should be 0
        expect(result.current.viewStyle).toBe(0);

        // Attempt to switch view style without an index
        act(() => result.current.onViewStyleSwitch({}));

        // Expect view style index to still be 0
        expect(result.current.viewStyle).toBe(0);
    });
});
