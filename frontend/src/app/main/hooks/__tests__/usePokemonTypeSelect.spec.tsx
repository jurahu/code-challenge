import { TYPE_ALL, usePokemonTypeSelect } from "@/app/main/hooks/usePokemonTypeSelect";
import { act, renderHook } from "@testing-library/react";

describe("usePokemonTypeSelect", () => {
    it('should initialize with default type "All"', () => {
        const { result } = renderHook(() => usePokemonTypeSelect());
        expect(result.current.selectedPokemonType).toBe(TYPE_ALL);
    });

    it("should update the selected type on onSelectType", () => {
        const { result } = renderHook(() => usePokemonTypeSelect());
        const newType = "Fire";
        const fakeOnChangeData = { selectedItem: newType }; // Mimic the data structure used by your dropdown

        act(() => {
            result.current.onSelectType(fakeOnChangeData);
        });

        expect(result.current.selectedPokemonType).toBe(newType);
    });

    it('should fallback to default type "All" if selectedItem in onSelectType is null or undefined', () => {
        const { result } = renderHook(() => usePokemonTypeSelect());
        const fakeOnChangeDataNull = { selectedItem: null };
        const fakeOnChangeDataUndefined = { selectedItem: undefined };

        act(() => {
            result.current.onSelectType(fakeOnChangeDataNull);
        });
        expect(result.current.selectedPokemonType).toBe(TYPE_ALL);

        act(() => {
            // @ts-ignore
            result.current.onSelectType(fakeOnChangeDataUndefined);
        });
        expect(result.current.selectedPokemonType).toBe(TYPE_ALL);
    });
});
