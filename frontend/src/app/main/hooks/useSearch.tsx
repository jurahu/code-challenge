import { useCallback, useState } from "react";
import { debounce } from "@/shared/debounce";

export function useSearch() {
    const [search, setSearch] = useState("");

    const onSearchChange = useCallback(
        debounce(function onSearchChange(event: { target: HTMLInputElement; type: "change" }) {
            setSearch(event.target.value);
        }, 50),
        [],
    );
    return { search, onSearchChange };
}