import { PokemonView } from "@/app/main/PokemonView";
import { getPokemons } from "@/actions/getPokemons";
import { toggleFavorite } from "@/actions/toggleFavorite";
import { getPokemonTypes } from "@/actions/getPokemonTypes";

export default async function Home() {
    const pokemons = await getPokemons();
    const pokemonTypes = await getPokemonTypes();
    return (
        <main>
            <PokemonView pokemonsInit={pokemons} toggleFavorite={toggleFavorite} pokemonTypes={pokemonTypes} />
        </main>
    );
}
