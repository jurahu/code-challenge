"use server";
import { getPokemonById } from "@/actions/getPokemonById";
import { PokemonDetail } from "@/app/detail/[id]/components/PokemonDetail";

export default async function Detail({ params }: { params: { id: string } }) {
    const pokemon = await getPokemonById(params.id);
    return (
        <main>
            <PokemonDetail pokemon={pokemon} />
        </main>
    );
}
