"use client";
import styles from "@/app/detail/[id]/components/PokemonDetail.module.scss";
import { Button, Grid, ProgressBar, Tile } from "@carbon/react";
import { TileImage } from "@/shared/components/TileImage";
import { Favorite, FavoriteFilled, VolumeUpFilled } from "@carbon/icons-react";
import { PokemonDetailResult } from "@/actions/getPokemonById";
import { useFavorite } from "@/app/detail/[id]/hooks/useFavorite";
import { useCallback, useRef, useState } from "react";
import { PokemonTile } from "@/shared/components/PokemonTile";
import { toggleFavorite } from "@/actions/toggleFavorite";
import { SwitchableColumn } from "@/shared/components/SwitchableColumn";

type PokemonDetailProps = { pokemon: PokemonDetailResult };

export function PokemonDetail({ pokemon }: PokemonDetailProps) {
    const { isFavorite, onFavoriteClick } = useFavorite(pokemon);
    const [evolutions, setEvolutions] = useState(pokemon.evolutions);
    const audio = useRef(new Audio(pokemon.sound));
    const onPlaySound = useCallback(async function () {
        await audio.current.play();
    }, []);
    const onToggleEvolutionFavorite = useCallback(
        async function (id: string, isFavorite: boolean) {
            await toggleFavorite(id, isFavorite);
            setEvolutions((evolutions) =>
                evolutions.map((evolution) =>
                    evolution.id === id
                        ? {
                              ...evolution,
                              isFavorite: !isFavorite,
                          }
                        : evolution
                )
            );
        },
        [evolutions]
    );

    return (
        <Tile className={styles.tile}>
            <div className={styles.imageWrapper}>
                <TileImage url={pokemon.image} name={pokemon.name} small={false} />
                <div className={styles.playButton}>
                    <Button onClick={onPlaySound} kind="ghost" aria-label="play sound" label="play sound" hasIconOnly={true}>
                        <VolumeUpFilled />
                    </Button>
                </div>
            </div>
            <div className={styles.flex}>
                <div className={styles.main}>
                    <h2>{pokemon.name}</h2>
                    <p>{pokemon.types.join(", ")}</p>
                </div>
                <div className={styles.right}>
                    <Button
                        hasIconOnly={true}
                        aria-label={isFavorite ? "Unfavorite" : "Favorite"}
                        label={isFavorite ? "Unfavorite" : "Favorite"}
                        onClick={onFavoriteClick}
                    >
                        {isFavorite ? <FavoriteFilled /> : <Favorite />}
                    </Button>
                </div>
            </div>
            <div className={styles.flex}>
                <div className={styles.main}>
                    <ProgressBar label={""} value={pokemon.maxCP} />
                </div>
                <div className={styles.right}>CP: {pokemon.maxCP}</div>
            </div>
            <div className={styles.flex}>
                <div className={styles.main}>
                    <ProgressBar label={""} value={pokemon.maxHP} />
                </div>
                <div className={styles.right}>HP: {pokemon.maxHP}</div>
            </div>
            <div className={styles.flex}>
                <div className={styles.weight}>
                    <div>
                        <strong>Weight</strong>
                    </div>
                    <div>
                        {pokemon.weight.minimum} - {pokemon.weight.maximum}
                    </div>
                </div>
                <div className={styles.height}>
                    <div>
                        <strong>Height</strong>
                    </div>
                    <div>
                        {pokemon.height.minimum} - {pokemon.height.maximum}
                    </div>
                </div>
            </div>
            <div className={styles.evolutions}>
                <h4>Evolutions</h4>
                <Grid>
                    {evolutions.map((pokemon) => (
                        <SwitchableColumn rowMode={false} key={pokemon.id}>
                            <PokemonTile pokemon={pokemon} onToggleFavorite={onToggleEvolutionFavorite} viewType="tile" />
                        </SwitchableColumn>
                    ))}
                </Grid>
            </div>
        </Tile>
    );
}
