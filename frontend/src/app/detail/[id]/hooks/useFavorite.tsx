import { useCallback, useEffect, useState } from "react";
import { toggleFavorite } from "@/actions/toggleFavorite";
import { getPokemonById } from "@/actions/getPokemonById";
import { Pokemon } from "@/api/graphql";

export function useFavorite(pokemon: Pick<Pokemon, "id" | "isFavorite">) {
    const [isFavorite, setIsFavorite] = useState(pokemon.isFavorite);
    const onFavoriteClick = useCallback(
        async function onFavoriteClick() {
            await toggleFavorite(pokemon.id, isFavorite);
            setIsFavorite(!isFavorite);
        },
        [isFavorite]
    );

    useEffect(() => {
        getPokemonById(pokemon.id).then((r) => setIsFavorite(r.isFavorite));
    }, []);

    return { isFavorite, onFavoriteClick };
}
