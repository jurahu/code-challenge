import type { Metadata } from "next";
import "./globals.scss";
import { ThemeSwitcher } from "@/shared/components/ThemeSwitcher";

export const metadata: Metadata = {
    title: "Pokemon",
    description: "Code challenge pokemon, carbon + next + typescript",
};

export default function RootLayout({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <html lang="en">
            <body>
                <ThemeSwitcher>{children}</ThemeSwitcher>
            </body>
        </html>
    );
}
