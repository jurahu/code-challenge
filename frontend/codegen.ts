import type { CodegenConfig } from "@graphql-codegen/cli";

const config: CodegenConfig = {
    overwrite: true,
    schema: "../backend/src/schema.graphql",
    generates: {
        "src/app/api/graphql.ts": {
            plugins: ["typescript", "typescript-document-nodes"],
        },
    },
};

export default config;
